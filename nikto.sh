#!/bin/bash

nikto -h $url -ssl -o ./test2/niktores -F xml
zaproxy -cmd -quickurl $url -quickout /test2/zapres
cd /usr/lib/dradis
bundle exec thor dradis:plugins:nikto:upload /test2/niktores
bundle exec thor dradis:plugins:zap:upload /test2/zapres
bundle exec thor dradis:plugins:html:export --output=/test2/res.html \
--template=/usr/lib/dradis/templates/reports/html_export/default_dradis_template_v3.0.html.erb
